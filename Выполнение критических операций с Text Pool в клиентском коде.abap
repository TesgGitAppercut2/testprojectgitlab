report zuploadtxt.
parameters:
    p_report like sy-repid,
    p_file like rlgrap-filename,
    p_lang like sy-langu.
data: it_text like textpool occurs 100 with header line.
start-of-selection.
    call function 'WS_UPLOAD'
        exporting
            filename            = p_file
            filetype            = 'DAT'
        tables
            data_tab            = it_text
        exceptions
            conversion_error    = 1
            file_open_error     = 2
            file_read_error     = 3
            invalid_table_width = 4
            invalid_type        = 5
            no_batch            = 6
            unknown_error       = 7
            others              = 8.
    if sy-subrc = 0.
        insert textpool p_report from it_text language p_lang.
        write: / 'insert textpool : ', sy-subrc.
    else.
        write: / 'ws_upload : ', sy-subrc.
    endif.