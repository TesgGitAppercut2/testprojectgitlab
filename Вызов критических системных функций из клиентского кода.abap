DATA: lv_len TYPE i.
DATA: lv_sqlerr TYPE i.
PARAMETERS lv_stmt TYPE c LENGTH 80.
lv_len = STRLEN( lv_stmt ).
CALL 'C_DB_EXECUTE' ID 'STATLEN' FIELD lv_len
    ID 'STATTXT' FIELD lv_stmt
    ID 'SQLERR' FIELD lv_sqlerr.