report zgendynpro
data: h type table of d020s with header line,
      f type table of d021s with header line,
      e type table of d022s with header line,
      m type table of d023s with header line.
data: mess(50) type c,
      line(50) type c,
      word(50) type c.
data: dynproid(44).
dynproid = sy-repid.
dynproid+40(4) = '0100'.
h-prog = sy-repid.
h-dnum = '0100'.
append h.
f-fnam = 'P_TEST1'.
f-flg1 = '80'.
f-flg3 = '80'.
f-line = '01'.
f-coln = '03'.
f-type = 'CHAR'.
f-ityp = 'C'.
f-leng = '20'.
f-stxt = '____________________'.
append f.
f-fnam = 'P_TEST2'.
f-flg1 = '80'.
f-flg3 = '80'.
f-line = '02'.
f-coln = '03'.
f-type = 'CHAR'.
f-ityp = 'C'.
f-leng = '40'.
f-stxt = '________________________________________'.
append f.
e-line = 'PROCESS BEFORE OUTPUT.'.
append e.
e-line = 'PROCESS AFTER INPUT.'.
append e.
generate dynpro h f e m id dynproid
         message mess
         line    line
         word    word.
call screen 100