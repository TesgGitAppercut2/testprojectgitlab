string login = Request["login"];
string password = Request["password"];
string url = Request["url"];
if (url == null)
{
	url = "/Mainpage";
}
if ((Session["id"] != null) || (Authenticate(login, password)))
{
	//Valid credentials
	Response.Redirect(url);
}
else
{
	//Invalid credentials
}